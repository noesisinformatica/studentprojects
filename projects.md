
# Components & Tools for supporting a scalable open source ecosystem

There is an increasing acknowledgement of the need to create an ecosystem of components that support healthcare delivery, following an increased adoption of open source tools in the wider IT community. This is exemplified by the recent [Open Digital Challenge Fund](http://rippleosi.org/open-digital-platform-challenge-fund/). There is also a focus on creating plausible demonstrator apps that leverage existing components based on international standads. The Code4Health platform is one such effort that allows a prospective app developer to provision a clincal data repository, a master patient index and a terminology server in a self service setting. The current [Code4Health platform](https://platform.code4health.org) is designed to provide:

 - A Clinical Data Repository based on the openEHR implementation by Marand
 - A Master Patient Index based on a proprietary implemention by Marand
 - A simple terminology service for non standard terminologies implemented by Marand

Of the three components, only the CDR is a complete offering. The other two are offered as placeholders for functionality that will ultimately be provided by dedicated offerings. Since any plausible clinical information system (CIS) would use dedicated components, it makes sense for the C4H platform to use adopt the dedicated offerings for MPI and terminology services. Within the context of wider open source efforts in the NHS and other projects like Ripple, there exist a set of components that are currently being used or could be used to serve this dedicated functionality. These components are listed below

 - [EtherCIS](https://github.com/ethercis/ethercis), open source openEHR implementation - currently in use with in the RippleOsi offering
 - [openEMPI](http://www.openempi.org/), an enterprise master patient index and demographics solution
 - [Termlex](https://v1.termlex.com/docs/), a terminology server that suppports both SNOMED CT and local code sets - currently in use in Ripple and PEACH projects.

The [UCL PEACH project](https://code4health.org/peach) already intends to use these these three components as the building blocks for a patient data store and an analytics solution built on top of it. Given the synergy in objectives betwen the C4H platform and PEACH, it makes sense to define dedicated projects that will ensure that the above components can be:

 - Integrated to deliver the functionality needed by PEACH or any future projects
   - Instead of designing custom middleware the enables communication between these components, it makes sense to adopt [HL7 FHIR](https://www.hl7.org/fhir/) as the basis for integration between these components. 
 - Packaged into plug and play artefacts that can fit into the C4H platform or any other platform
   - Along with FHIR, packaging up the components into Docker based containers allows these components to be easily deployed into any platform.

With this background and longer term vision in mind, a series of projects can now be offered to students. These can be split into two broad groups.
 - Infrastructure projects: This group of projects involves
   - Containerising (via Docker) the three components mentioned above - EtherCis, OpenEMPI and Termlex
   - Integration between EtherCis, OpenEMPI and Termlex using FHIR as the interface layer, instead of using a custom layer. In fact, a custom middleware was created by the previous group but there are a number of design issues with this integration. Hence the scope of the infrastructure projects would be to design and then deliver a resilient, RESTful integration between EtherCis, OpenEMPI and Termlex.
 - Platform projects: This group of projects involve augmenting the set of tools, samples available for the Code4Health platform.
   - The Code4Health exposes a series of APIs which can be used to build apps. Documentation for these APIs is also avaialble. However, app developers are more likely to find it useful if they have a series of 'hello world' starter apps to build/learn from. 
   - It will also be helpful for Code4Health app developers to be able to use 'template' projects to base their own projects from. So the scope of 'platform projects' will be to document, create and deliver a series of hello world apps in at least two programming languages - and then generate associated stubs/starter projects for the programming languages selected.

## Infrastructure projects

### Containerisation of EtherCIS
 - Status: delivered?
 - Existing work: [https://gitlab.com/ucl-peach/docker-ethercis](https://gitlab.com/ucl-peach/docker-ethercis)

### Containerisation of OpenEMPI
 - Status: delivered?
 - Existing work: [https://gitlab.com/ucl-peach/docker-openempi](https://gitlab.com/ucl-peach/docker-openempi)

### Containerisation of Termlex
 - Status: delivered

### FHIR wrapper for EtherCIS
 - Status: Not delivered
 - Available: Yes
 - Scope: This is a larger piece of work that requires understandind of both the openEHR and FHIR specs. 
 - Recommendation: Not recommended for a student project

### FHIR wrapper for openEMPI
 - Status: Not delivered
 - Available: Yes
 - Scope: This is a well encapsulated piece of work that requires alignment of openEMPI APIs with FHIR specs.
 - Recommendation: Recommended for a student project

### FHIR wrapper for Termlex
 - Status: Not delivered
 - Available: Yes
 - Scope: This is a well encapsulated piece of work that requires alignment of Termlex APIs with FHIR specs.
 - Recommendation: Recommended for a student project, with guidance

### Multi-tenancy support for EtherCIS
 - Status: Not delivered
 - Available: Yes
 - Scope: This is a well encapsulated piece of work that requires investigation into the EtherCIS backend & possibly tweaking of the PostgreSQL engine.
 - Recommendation: A challenging student project

## Platform projects

### openEhr provisioning and mock data generation tool
 - Status: Exists, need rewrite
 - Available: No
 - Scope: This is a well encapsulated piece of work that requires investigation into the existing NodeJs and AngularJs app
 - Recommendation: Recommended for a student project
 - Existing work: [https://github.com/operonsystems/ehrscape-provisioner](https://github.com/operonsystems/ehrscape-provisioner)
 - Background: When a CDR and MPI are provisioned on the current Code4Health platform, a series of scripts & calls are executed that populate the CDR with mock patient data. There is value in extending these calls to create mock data for specific domains and associated terminology resources. 

### Hello World project
 - Status: Not delivered
 - Available: Yes
 - Scope: There is a lot of value in creating a simple 'hello world' project once the dockerised MPI, openEHR and terminology server are running in the platform. There could be various flavours of 'Hello world' applications in different programming languages. Since all our components are REST based and will conform to FHIR, the student can pick a programming language of choice.
 - Recommendation: A good student project, exposing the student to various generic web technologies and also health informatics.

### Templated project/app stub generation
 - Status: Not delivered
 - Available: Yes
 - Scope: A lot of mature development frameworks and IDEs use the notion of templates/archetypes to generate application stubs. For example you can easily use Maven archetype or Npm generator to create a webapp with standardised structure that already includes a collection of necessary resources. The layout of these resources and their use could also incorportate good design principles. So in effect using a 'templated' project would allow a developer to not just create an app quickly, but also ensure that it follows good practices. 
 - Recommendation: A good student project, exposing the student to various templating and stub generation technologies. 
