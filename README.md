# Information

A repo that contains a list of possible Msc student projects. The markdown source is located at[https://gitlab.com/noesisinformatica/studentprojects/blob/master/projects.md](https://gitlab.com/noesisinformatica/studentprojects/blob/master/projects.md)

You can download the Word and Pdf versions using the links below. They are automatically created and added to the `output` branch of this repo.

 - [Word version](https://gitlab.com/noesisinformatica/studentprojects/raw/output/projects.docx)
 - [Pdf version](https://gitlab.com/noesisinformatica/studentprojects/raw/output/projects.pdf)